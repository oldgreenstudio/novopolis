from django.urls import path

from nps_site import views
from nps_site.decorators import check_recaptcha

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('company/', views.CompanyView.as_view(), name="company"),
    path('houses/', views.HousesView.as_view(), name="houses"),
    path('docs/', views.DocsView.as_view(), name="docs"),
    path('phones/', views.PhonesView.as_view(), name="phones"),
    path('gallery/', views.GalleryView.as_view(), name="gallery"),
    path('message/', check_recaptcha(views.MessageView.as_view()), name="message"),
    path('application/', check_recaptcha(views.ApplicationView.as_view()), name="application"),
]