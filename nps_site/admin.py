from django.contrib import admin

from nps_site.models import Company, News, Employee, House, Document, Contract, Letter, Application

admin.site.register(Company)
admin.site.register(News)
admin.site.register(Employee)
admin.site.register(House)
admin.site.register(Document)
admin.site.register(Contract)
admin.site.register(Letter)
admin.site.register(Application)