from functools import wraps

import requests
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView, RedirectView, CreateView

from Novopolis import settings
from nps_site.decorators import check_recaptcha
from nps_site.forms import LetterForm, ApplicationForm
from nps_site.models import News, House, Employee, Contract, Document, Company, Letter, Application


class IndexView(ListView):
    template_name = "nps_site/index.html"
    context_object_name = "news"

    def get_queryset(self):
        return News.objects.all()[:5]

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context


class CompanyView(ListView):
    template_name = "nps_site/about.html"
    context_object_name = "query"

    def get_template_names(self):
        page = self.request.GET.get("page")
        template_name = "nps_site/about.html"
        print(page)

        if page == "requisites":
            template_name = "nps_site/requisites.html"
        elif page == "leadership":
            template_name = "nps_site/leadership.html"
        elif page == "email":
            template_name = "nps_site/email.html"

        return template_name

    def get_queryset(self):
        page = self.request.GET.get("page")
        query = None

        if page == "leadership":
            query = Employee.objects.all()

        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context


class HousesView(ListView):
    template_name = "nps_site/houses.html"
    context_object_name = "houses"

    def get_queryset(self):
        return House.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context


class DocsView(ListView):
    template_name = "nps_site/passtable.html"
    context_object_name = "query"

    def get_template_names(self):
        page = self.request.GET.get("page")
        template_name = "nps_site/passtable.html"

        if page == "contracts":
            template_name = "nps_site/contracts.html"

        return template_name

    def get_queryset(self):
        page = self.request.GET.get("page")
        query = Document.objects.filter(company=True)

        if page == "contracts":
            query = Contract.objects.all()

        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context


class PhonesView(TemplateView):
    template_name = "nps_site/numbers.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context


class GalleryView(RedirectView):
    url = "https://vk.com/albums-173072410"


class MessageView(SuccessMessageMixin, CreateView):
    model = Letter
    form_class = LetterForm
    template_name = "nps_site/message.html"
    success_url = reverse_lazy("message")
    success_message = "Ваше сообщение отправлено!"

    def form_valid(self, form):
        # проверка валидности reCAPTCHA
        if self.request.recaptcha_is_valid:
            return super().form_valid(form)
        return render(self.request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context


class ApplicationView(SuccessMessageMixin, CreateView):
    model = Application
    form_class = ApplicationForm
    template_name = "nps_site/application.html"
    success_url = reverse_lazy("application")
    success_message = "Ваша заявка отправлена!"

    def form_valid(self, form):
        # проверка валидности reCAPTCHA
        if self.request.recaptcha_is_valid:
            return super().form_valid(form)
        return render(self.request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["company"] = Company.objects.first()
        return context
