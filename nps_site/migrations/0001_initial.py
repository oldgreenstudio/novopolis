# Generated by Django 3.0.3 on 2020-02-09 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Название')),
                ('address', models.CharField(max_length=64, verbose_name='Адрес')),
                ('phone1', models.CharField(max_length=64, verbose_name='Телефон')),
                ('phone2', models.CharField(blank=True, max_length=64, null=True, verbose_name='Доп. телефон')),
                ('email', models.EmailField(max_length=64, verbose_name='Почта')),
            ],
            options={
                'verbose_name': 'Компания',
                'verbose_name_plural': 'Компании',
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Название')),
                ('file', models.FileField(upload_to='documents', verbose_name='Документ')),
            ],
            options={
                'verbose_name': 'Документ',
                'verbose_name_plural': 'Документы',
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fio', models.CharField(max_length=64, verbose_name='ФИО')),
                ('position', models.CharField(max_length=64, verbose_name='Должность')),
                ('image', models.ImageField(upload_to='employees', verbose_name='Аватар')),
            ],
            options={
                'verbose_name': 'Сотрудник',
                'verbose_name_plural': 'Сотрудники',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('caption', models.CharField(max_length=128, verbose_name='Заголовок')),
                ('image', models.ImageField(upload_to='news/%Y_%m_%d', verbose_name='Обложка')),
                ('text', models.TextField(verbose_name='Текст')),
            ],
            options={
                'verbose_name': 'Новость',
                'verbose_name_plural': 'Новости',
            },
        ),
        migrations.CreateModel(
            name='House',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=64, verbose_name='Адрес')),
                ('type', models.CharField(max_length=2, verbose_name='Вид обслуживания')),
                ('docs', models.ManyToManyField(related_name='houses', to='nps_site.Document', verbose_name='Документы')),
            ],
            options={
                'verbose_name': 'Дом',
                'verbose_name_plural': 'Дома',
            },
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Название')),
                ('docs', models.ManyToManyField(related_name='contracts', to='nps_site.Document', verbose_name='Документы')),
            ],
            options={
                'verbose_name': 'Договор',
                'verbose_name_plural': 'Договоры',
            },
        ),
    ]
