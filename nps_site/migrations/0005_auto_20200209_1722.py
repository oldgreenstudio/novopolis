# Generated by Django 3.0.3 on 2020-02-09 14:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nps_site', '0004_auto_20200209_1709'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contract',
            options={'ordering': ['name'], 'verbose_name': 'Договор', 'verbose_name_plural': 'Договоры'},
        ),
        migrations.AlterModelOptions(
            name='document',
            options={'ordering': ['name'], 'verbose_name': 'Документ', 'verbose_name_plural': 'Документы'},
        ),
        migrations.AlterModelOptions(
            name='employee',
            options={'ordering': ['fio'], 'verbose_name': 'Сотрудник', 'verbose_name_plural': 'Сотрудники'},
        ),
        migrations.AlterModelOptions(
            name='house',
            options={'ordering': ['address'], 'verbose_name': 'Дом', 'verbose_name_plural': 'Дома'},
        ),
        migrations.AlterField(
            model_name='document',
            name='company',
            field=models.BooleanField(default=False, verbose_name='Паспортный стол'),
        ),
        migrations.AlterField(
            model_name='house',
            name='type',
            field=models.CharField(choices=[('CN', 'Управление')], max_length=2, verbose_name='Вид обслуживания'),
        ),
    ]
