# Generated by Django 3.0.3 on 2020-02-09 22:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nps_site', '0008_application_letter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='time',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Желаемое время прихода специалиста'),
        ),
    ]
