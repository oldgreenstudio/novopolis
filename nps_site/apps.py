from django.apps import AppConfig


class NpsSiteConfig(AppConfig):
    name = 'nps_site'
