from django.db import models
from django.utils.timezone import now

HOUSE_TYPE_CHOICES = [
    ("CN", "Управление"),
]

LETTER_TYPE_CHOICES = [
    ("A", "Заявление"),
    ("C", "Жалоба"),
    ("O", "Предложение"),
    ("N", "Уведомление"),
    ("P", "Претензия"),
    ("G", "Благодарность"),
    ("L", "Письмо"),
]


class Company(models.Model):
    name = models.CharField(max_length=64, verbose_name="Название")
    address = models.CharField(max_length=64, verbose_name="Адрес")
    phone1 = models.CharField(max_length=64, verbose_name="Телефон")
    phone2 = models.CharField(max_length=64, null=True, blank=True, verbose_name="Доп. телефон")
    email = models.EmailField(max_length=64, verbose_name="Почта")

    def __str__(self):
        return self.name

    def phone(self):
        if self.phone2 is None:
            return self.phone1
        return "{}, {}".format(self.phone1, self.phone2)

    class Meta:
        verbose_name = "Компания"
        verbose_name_plural = "Компании"


class News(models.Model):
    caption = models.CharField(max_length=128, verbose_name="Заголовок")
    image = models.ImageField(upload_to="news/%Y_%m_%d", verbose_name="Обложка")
    text = models.TextField(verbose_name="Текст")
    date = models.DateField(default=now, verbose_name="Дата")

    def __str__(self):
        return self.caption

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
        ordering = ["-date"]


class Employee(models.Model):
    fio = models.CharField(max_length=64, verbose_name="ФИО")
    position = models.CharField(max_length=64, verbose_name="Должность")
    image = models.ImageField(upload_to="employees", null=True, blank=True, verbose_name="Аватар")

    def __str__(self):
        return self.fio

    class Meta:
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"
        ordering = ["fio"]


class Document(models.Model):
    name = models.CharField(max_length=64, verbose_name="Название")
    file = models.FileField(upload_to="documents", verbose_name="Документ")
    company = models.BooleanField(default=False, verbose_name="Паспортный стол")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Документ"
        verbose_name_plural = "Документы"
        ordering = ["name"]


class House(models.Model):
    address = models.CharField(max_length=64, verbose_name="Адрес")
    type = models.CharField(max_length=2, choices=HOUSE_TYPE_CHOICES, verbose_name="Вид обслуживания")
    docs = models.ManyToManyField(Document, blank=True, related_name="houses", verbose_name="Документы")

    def __str__(self):
        return self.address

    class Meta:
        verbose_name = "Дом"
        verbose_name_plural = "Дома"
        ordering = ["address"]


class Contract(models.Model):
    name = models.CharField(max_length=64, verbose_name="Название")
    docs = models.ManyToManyField(Document, blank=True, related_name="contracts", verbose_name="Документы")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Договор"
        verbose_name_plural = "Договоры"
        ordering = ["name"]


class Letter(models.Model):
    fio = models.CharField(max_length=64, verbose_name="ФИО отправителя")
    address = models.CharField(max_length=64, null=True, blank=True, verbose_name="Адрес")
    phone = models.CharField(max_length=64, null=True, blank=True, verbose_name="Контактный телефон")
    email = models.EmailField(max_length=64, verbose_name="Электронная почта")
    type = models.CharField(max_length=1, choices=LETTER_TYPE_CHOICES, verbose_name="Тип обращения")
    topic = models.CharField(max_length=64, verbose_name="Тема сообщения")
    text = models.TextField(verbose_name="Сообщение")
    datetime = models.DateTimeField(default=now, verbose_name="Дата и время создания")

    def __str__(self):
        return "{} - {} - {}".format(self.fio, self.get_type_display(), self.topic)

    class Meta:
        verbose_name = "Обращение"
        verbose_name_plural = "Обращения"
        ordering = ["-datetime"]


class Application(models.Model):
    fio = models.CharField(max_length=64, verbose_name="ФИО отправителя")
    phone = models.CharField(max_length=64, verbose_name="Контактный телефон")
    address = models.CharField(max_length=64, verbose_name="Адрес")
    apartment = models.IntegerField(verbose_name="Номер квартиры")
    email = models.EmailField(max_length=64, verbose_name="Электронная почта")
    description = models.TextField(verbose_name="Описание заявки")
    time = models.CharField(max_length=128, null=True, blank=True, verbose_name="Желаемое время прихода специалиста")
    datetime = models.DateTimeField(default=now, verbose_name="Дата и время создания")

    def cut(self):
        cut = self.description
        if len(self.description) > 32:
            cut = self.description[:28] + "..."
        return cut

    def __str__(self):
        return "{} - {} - {}".format(self.fio, self.address, self.cut())

    class Meta:
        verbose_name = "Заявка"
        verbose_name_plural = "Заявки"
        ordering = ["-datetime"]
